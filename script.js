const socket = io()

socket.on("chat-message", data =>{
    appendMessage(data.name + ": " + data.message);
    
})
socket.on("user-connected", name =>{
    appendMessage(name + ' connected to the channel!');
    
})
socket.on("user-disconnected", name =>{
    appendMessage(name + ' disconnected from the channel.');
    
})

var messageContainer = document.getElementById("message-container")
var messageForm = document.getElementById("send-container");
var messageInput = document.getElementById("message-input")
name = prompt("Enter your name: ")
appendMessage("Welcome to the chat!")
socket.emit("new-user", name)

messageForm.addEventListener("submit", e=>{
    e.preventDefault()
    message = messageInput.value
    socket.emit("send-chat-message", message)
    appendMessage("You: " + message, true);
    messageInput.value = ""
})

function appendMessage(message, sender){
 const messageElement = document.createElement("p")
 messageElement.innerText = message
 messageContainer.append(messageElement)
 if(sender){
        messageElement.classList.add("you")
}
}