const path = require('path');
const http = require('http');
const express = require('express');
const socketIO = require('socket.io');

const publicPath = path.join(__dirname, '../public');
const port = process.env.PORT || 3000;
var app = express();
var server = http.createServer(app);
io = socketIO(server);

app.use(express.static(__dirname));

// const io = require("socket.io")(process.env.PORT || 80)

const users = {}
io.on("connection", socket =>{
    socket.on("send-chat-message", message=>{
        socket.broadcast.emit("chat-message", {message,name:users[socket.id]})
    })
    socket.on("new-user", name=>{
        users[socket.id] = name
        socket.broadcast.emit("user-connected", name)
    })
    socket.on("disconnect", name=>{
        socket.broadcast.emit("user-disconnected", users[socket.id])
        delete users[socket.id]
       
    })
})

server.listen(port, () => {
  console.log(`Server is up on ${port}`);
});